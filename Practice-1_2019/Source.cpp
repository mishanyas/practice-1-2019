#include <stdio.h>
#include <locale.h>
#include <iostream>
#include <time.h>
#define SIZE 1024

void delete_punctuation(char*, char);

int main()
{
	setlocale(LC_CTYPE, "rus");

	char text[SIZE] = { '0' };
	char signs[] = { ' ','.',',','!','?','-',':','_','(',')', ';', '<', '>' };
	printf_s("������� �����! (������ �� 1024 �������)\n��� �����: ");
	gets_s(text, SIZE);

	for (int i = 0; i < 13; i++)
	{
		delete_punctuation(text, signs[i]);
	}

	int size_txt = 0;
	size_txt = strlen(text);

	int* mass_leter_num = (int*)calloc(size_txt, sizeof(int));
	char* new_text = (char*)malloc(size_txt);
	
	for (int i = 0; i < size_txt; i++)
	{
		mass_leter_num[i] = size_txt;
		new_text[i] = { '0' };
	}
	
	srand((int) time(NULL));

	for (int i = 0; i < size_txt;)
	{
		int num = 0;
		num = rand() % size_txt;

		if (mass_leter_num[num] == size_txt)
		{
			mass_leter_num[num] = i;
			new_text[num] = text[i];
			i++;
		}
	}

	new_text[size_txt] = { '\0' };

	printf_s("��������� �������� ��������� �������: ");
	puts(new_text);

	system("pause");

	return 0;
}

void delete_punctuation(char* text, char sign)
{
	char* label_1 = text, *label_2 = text;

	while (1)
	{
		label_1 = strchr(text, sign);

		if (label_1 == NULL)
		{
			return;
		}

		label_2 = label_1 + 1;

		for (; (*label_1) != '\0'; label_1++)
		{
			(*label_1) = (*label_2);
			label_2++;
		}

		(*label_1) = '\0';
	}	
}